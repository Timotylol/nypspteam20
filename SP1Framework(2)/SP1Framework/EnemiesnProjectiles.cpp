//This file is for enemy and projectile movement

#include "Framework\console.h"
#include "game.h"
#include "map.h"



/*
	if (direction == 1)
	{
		--g_Enemy[0].m_cLocation.Y; //UP
	}
	else if (direction == 2)
	{
		++g_Enemy[0].m_cLocation.Y; //DOWN
	}
	else if (direction == 3)
	{
		--g_Enemy[0].m_cLocation.X; //LEFT
	}
	else if (direction == 4)
	{
		++g_Enemy[0].m_cLocation.X; //RIGHT
	}




	
	struct EnemyCoordinateLimits
	{
		int MaxX;
		int MaxY;
	} Enemy[3];

	Enemy[0].MaxX = 9;
	Enemy[0].MaxY = 3;

	int direction = 0;

	if (Enemy[0].MaxX == 9 && Enemy[0].MaxY <= 2) //Goes up
	{
		direction = 1;
		--Enemy[0].MaxY;
//		--g_Enemy[0].m_cLocation.Y;
	}
	else if (Enemy[0].MaxX == 0 && Enemy[0].MaxY <= 2) //Goes down
	{
		direction = 2;
		++Enemy[0].MaxY;
//		++g_Enemy[0].m_cLocation.Y;
	}
	else if (Enemy[0].MaxX <= 8 && Enemy[0].MaxY == 0) //Goes left
	{
		direction = 3;
		--Enemy[0].MaxX;
//		--g_Enemy[0].m_cLocation.X;
	}
	else if (Enemy[0].MaxX <= 8 && Enemy[0].MaxY == 3) //Goes right
	{
		direction = 4;
		++Enemy[0].MaxX;
//		++g_Enemy[0].m_cLocation.X;
	}

	return g_Enemy[0].m_cLocation;

	//g_Enemy[0].m_cLocation;
	*/
	/*
	if (g_Enemy[0].m_cLocation.Y > g_sChar.m_cLocation.Y && g_Enemy[0].m_cLocation.X == g_sChar.m_cLocation.X ||
	g_Enemy[0].m_cLocation.Y > g_sChar.m_cLocation.Y ||
	maps.Map[g_Enemy[0].m_cLocation.Y + 1][g_Enemy[0].m_cLocation.X] == 'x' )
	{
	direction = 1;
	}
	else if (g_Enemy[0].m_cLocation.Y < g_sChar.m_cLocation.Y && g_Enemy[0].m_cLocation.X == g_sChar.m_cLocation.X ||
	g_Enemy[0].m_cLocation.Y < g_sChar.m_cLocation.Y ||
	maps.Map[g_Enemy[0].m_cLocation.Y - 1][g_Enemy[0].m_cLocation.X] == 'x')
	{
	direction = 2;
	}
	else if (g_Enemy[0].m_cLocation.X > g_sChar.m_cLocation.X && g_Enemy[0].m_cLocation.Y == g_sChar.m_cLocation.Y ||
	g_Enemy[0].m_cLocation.X > g_sChar.m_cLocation.X ||
	maps.Map[g_Enemy[0].m_cLocation.Y][g_Enemy[0].m_cLocation.X + 1] == 'x' )
	{
	direction = 3;
	}
	else if (g_Enemy[0].m_cLocation.X < g_sChar.m_cLocation.X && g_Enemy[0].m_cLocation.Y == g_sChar.m_cLocation.Y ||
	g_Enemy[0].m_cLocation.X < g_sChar.m_cLocation.X ||
	maps.Map[g_Enemy[0].m_cLocation.Y][g_Enemy[0].m_cLocation.X - 1] == 'x' )
	{
	direction = 4;
	}


	[17:55, 20/08/2019] Raphael Phua: void gameplay()            // gameplay logic
	{
	processUserInput(); // checks if you should change states or do something else with the game, e.g. pause, exit
	thread t1(moveCharacter);    // moves the character, collision detection, physics, etc
	thread t2 (movebullet);
	thread t3 (moveEnemy0Map1);
	thread t4 (moveEnemy1Map1);
	thread t5 (moveEnemy2Map1);

	t1.join();
	t2.join();
	t3.join();
	t4.join();
	t5.join();
	}
	[17:55, 20/08/2019] Raphael Phua: void moveEnemy0Map1()
	{
	int direction = 0, Ymax = 3, Ymin = 0;
	float speed = 0.5;

	if (g_Enemy[0].m_cLocation.Y - 1 != Ymin && maps.Map[g_Enemy[0].m_cLocation.Y][g_Enemy[0].m_cLocation.X + 1] == 'x')
	{
	direction = -2;
	}
	else if (g_Enemy[0].m_cLocation.Y - 1 == Ymin && maps.Map[g_Enemy[0].m_cLocation.Y][g_Enemy[0].m_cLocation.X - 1] != 'x')
	{
	direction = -4;
	}
	else if (g_Enemy[0].m_cLocation.Y + 1 != Ymax && maps.Map[g_Enemy[0].m_cLocation.Y][g_Enemy[0].m_cLocation.X - 1] == 'x')
	{
	direction = 2;
	}
	else if (g_Enemy[0].m_cLocation.Y + 1 == Ymax && maps.Map[g_Enemy[0].m_cLocation.Y][g_Enemy[0].m_cLocation.X + 1] != 'x')
	{
	direction = 4;
	}

	COORD preCord = g_Enemy[0].m_cLocation;

	g_Enemy[0].m_cLocation = movement(&direction, g_Enemy[0].m_cLocation, speed, g_dElapsedTime, &g_dBounceTimeB, maps);

	UpdateMap(preCord, g_Enemy[0].m_cLocation, 'E');

	}

	void moveEnemy1Map1()
	{
	int direction = 0, Xmax = 37, Xmin = 32;
	float speed = 0.005;

	if (g_Enemy[1].m_cLocation.X + 1 == Xmax && maps.Map[g_Enemy[1].m_cLocation.Y - 1][g_Enemy[1].m_cLocation.X] != 'x')
	{
	direction = -2;
	}
	else if (maps.Map[g_Enemy[1].m_cLocation.Y - 1][g_Enemy[1].m_cLocation.X] == 'x' && g_Enemy[1].m_cLocation.X - 1 != Xmin)
	{
	direction = -4;
	}
	else if (g_Enemy[1].m_cLocation.X - 1 == Xmin && maps.Map[g_Enemy[1].m_cLocation.Y + 1][g_Enemy[1].m_cLocation.X] != 'x')
	{
	direction = 2;
	}
	else if (maps.Map[g_Enemy[1].m_cLocation.Y + 1][g_Enemy[1].m_cLocation.X] == 'x' && g_Enemy[1].m_cLocation.X + 1 != Xmax)
	{
	direction = 4;
	}

	COORD preCord = g_Enemy[1].m_cLocation;

	g_Enemy[1].m_cLocation = movement(&direction, g_Enemy[1].m_cLocation, speed, g_dElapsedTime, &g_dBounceTimeB, maps);

	UpdateMap(preCord, g_Enemy[1].m_cLocation, 'E');

	}

	void moveEnemy2Map1()
	{
	int direction = 0, Ymax = 24, Ymin = 21;
	float speed = 0.5;

	if (g_Enemy[2].m_cLocation.Y - 1 != Ymin && maps.Map[g_Enemy[2].m_cLocation.Y][g_Enemy[2].m_cLocation.X + 1] == 'x')
	{
	direction = -2;
	}
	else if (g_Enemy[2].m_cLocation.Y - 1 == Ymin && maps.Map[g_Enemy[2].m_cLocation.Y][g_Enemy[2].m_cLocation.X - 1] != 'x')
	{
	direction = -4;
	}
	else if (g_Enemy[2].m_cLocation.Y + 1 != Ymax && maps.Map[g_Enemy[2].m_cLocation.Y][g_Enemy[2].m_cLocation.X - 1] == 'x')
	{
	direction = 2;
	}
	else if (g_Enemy[2].m_cLocation.Y + 1 == Ymax && maps.Map[g_Enemy[2].m_cLocation.Y][g_Enemy[2].m_cLocation.X + 1] != 'x')
	{
	direction = 4;
	}

	COORD preCord = g_Enemy[2].m_cLocation;

	g_Enemy[2].m_cLocation = movement(&direction, g_Enemy[2].m_cLocation, speed, g_dElapsedTime, &g_dBounceTimeB, maps);

	UpdateMap(preCord, g_Enemy[2].m_cLocation, 'E');
	}
	[17:56, 20/08/2019] Raphael Phua: In game.h:
	[17:56, 20/08/2019] Raphael Phua: void moveEnemy0Map1();
	void moveEnemy1Map1();
	void moveEnemy2Map1();
	*/
