#include "Framework\console.h"
#include "game.h"
using namespace std;
bool CollisionOut(struct SGameChar *Collide_Object, struct SGameChar *current_Object, struct Maps maps);
bool Collision(struct SGameChar *current_Object, int *directions, struct Maps maps, vector<SGameChar*> AllChar);
int counts = 0;

void assigncount(int size)
{
	counts = size;
}
bool Collision(struct SGameChar *current_Object, int *directions, struct Maps maps, vector<SGameChar*> AllChar)
{
	bool CollidewithChar = false;
	COORD facing;
	if (((*directions) / 2) % 2 != 0)//direction 2,-2  == down,up  %2 instead of stoping at /2 cuz 1 and -1 are diff
	{
		facing.X = (*current_Object).m_cLocation.X;
		facing.Y = (*current_Object).m_cLocation.Y + (*directions) / 2;
		if (facing.X < 0 || facing.Y < 0 || facing.Y>24)
		{
			(*current_Object).isdestroyed;
			return 1;
		}
		for (int i = 0; i < AllChar.size(); i++)
			if ((*AllChar[i]).m_cLocation.X == facing.X && (*AllChar[i]).m_cLocation.Y == facing.Y)
			{
				return CollisionOut(&(*AllChar[i]), &(*current_Object), maps);
				CollidewithChar = true;
			}
			else if ((*AllChar[i]).m_cLocation.X == (*current_Object).m_cLocation.X && (*AllChar[i]).m_cLocation.Y == (*current_Object).m_cLocation.X && (*AllChar[i]).char_s != (*current_Object).char_s)
			{
				if (maps.Map[facing.Y][facing.X] == 'x')
				{
					if ((*current_Object).char_s == 'B')
						(*current_Object).isdestroyed = true;
					return 1;
				}
				return CollisionOut(&(*AllChar[i]), &(*current_Object), maps);
			}
		if (CollidewithChar == false)
		{
			if (maps.Map[facing.Y][facing.X] != ' ')
			{
				if (counts == 0)
				{
					if (maps.Map[facing.Y][facing.X] == 'y' || maps.Map[facing.Y][facing.X] == 'N')
					{
						return 0;
					}
				}
				if (maps.Map[(*current_Object).m_cLocation.Y][(*current_Object).m_cLocation.X] == 'B')
					(*current_Object).isdestroyed = true;
				return 1;
			}
		}
	}
	else//direction 4,-4 == right,left
	{
		facing.X = (*current_Object).m_cLocation.X + (*directions) / 4;
		facing.Y = (*current_Object).m_cLocation.Y;
		if (facing.X < 0 || facing.Y < 0 || facing.Y>maps.Map.size())
			return 0;
		for (int i = 0; i < AllChar.size(); i++)
		{
			if ((*AllChar[i]).m_cLocation.X == facing.X && (*AllChar[i]).m_cLocation.Y == facing.Y)
			{
				return CollisionOut(&(*AllChar[i]), &(*current_Object), maps);
				CollidewithChar = true;
			}
			else if ((*AllChar[i]).m_cLocation.X == (*current_Object).m_cLocation.X && (*AllChar[i]).m_cLocation.Y == (*current_Object).m_cLocation.X && (*AllChar[i]).char_s != (*current_Object).char_s)
			{
				if (maps.Map[facing.Y][facing.X] == 'x')
				{
					if ((*current_Object).char_s == 'B')
						(*current_Object).isdestroyed = true;
					return 1;
				}
				return CollisionOut(&(*AllChar[i]), &(*current_Object), maps);
			}
		}
		if (CollidewithChar == false)
			if (maps.Map[facing.Y][facing.X] != ' ')
			{
				if (counts == 0)
				{
					if (maps.Map[facing.Y][facing.X] == 'y' || maps.Map[facing.Y][facing.X] == 'N')
					{
						return 0;
					}
				}
				if (maps.Map[(*current_Object).m_cLocation.Y][(*current_Object).m_cLocation.X] == 'B')
					(*current_Object).isdestroyed = true;
				return 1;
			}
	}
	return 0;
}

bool CollisionOut(struct SGameChar *Collide_Object, struct SGameChar *current_Object, struct Maps maps)
{//Collision detection using location and the char it is assign to
	switch ((*Collide_Object).char_s)
	{
	case 'B':
		switch ((*current_Object).char_s)
		{//destroy enemy and bullet
		case 'E':
			if ((*Collide_Object).isreflected == true)
			{
				(*current_Object).isdestroyed = true;
				(*Collide_Object).isdestroyed = true;
				return 1;
			}
			break;
		case 'P':
			// minus Health
			if ((*Collide_Object).isreflected == true)
				return 0;
			(*Collide_Object).isdestroyed = true;
			HealthDrop();
			return 1;
			break;
		}
		break;
	case 'S':
		switch ((*current_Object).char_s)
		{
		case 'B':
			(*current_Object).isreflected = true;
			(*current_Object).direction = (*Collide_Object).direction;
			(*current_Object).m_cLocation = (*Collide_Object).m_cLocation;
			break;
		}
		break;

	case 'E':
		switch ((*current_Object).char_s)
		{
		case'B':
			if ((*current_Object).isreflected == true)
			{
				(*current_Object).isdestroyed = true;
				(*Collide_Object).isdestroyed = true;
				return 1;
			}
			break;
			//destroy enemy and bullet
		case'P':
			HealthDrop();
			break;
			// minus Health bring player back to start 
		}
		break;

	case 'P':
		switch ((*current_Object).char_s)
		{
		case'E':
			HealthDrop();
			return 0;
			break;
			//minus HEalth
		case 'B':
			if ((*current_Object).isreflected == true)
				return 0;
			(*current_Object).isdestroyed = true;
			HealthDrop();
			return 1;
			break;
			// minus Health bring player back to start 
		}
		break;

		/*case 'N':
			if (maps.Map[(*current_Object).m_cLocation.Y][(*current_Object).m_cLocation.X] == 'P')
			{
				return 0;
			}*/
			//if (current == 'B');
		break;
	}
	return 0;
}
double bounceT;
void movement(int *direction, struct SGameChar *current_Object, float speed, double elapsedT, double *bounceT, struct Maps *maps, vector<SGameChar*> AllChar)
{
	COORD location;
	location = (*current_Object).m_cLocation;
	bool bSomethingHappened = false;
	if (*bounceT > elapsedT)
	{
		//return (*current_Object).m_cLocation;
	}
	else
	{
		// Updating the location of the character based on the key press
		// providing a beep sound whenver we shift the character
		if (*direction == -2 && !Collision(&(*current_Object), &(*direction), (*maps), AllChar))
		{
			//maps.Map[current_location.Y, current_location.X] = 'P';
			location.Y--;
			bSomethingHappened = true;
		}
		else if (*direction == 2 && !Collision(&(*current_Object), &(*direction), (*maps), AllChar))
		{
			location.Y++;
			bSomethingHappened = true;
		}
		else if (*direction == -4 && !Collision(&(*current_Object), &(*direction), (*maps), AllChar))
		{
			location.X--;
			bSomethingHappened = true;
		}
		else if (*direction == 4 && !Collision(&(*current_Object), &(*direction), (*maps), AllChar))
		{
			location.X++;
			bSomethingHappened = true;
		}
		else
		{
			//return (*current_Object).m_cLocation;
		}
	}
	if (bSomethingHappened)
	{
		*bounceT = elapsedT + speed;
	}
	if (!(*current_Object).isdestroyed)
	{
		(*current_Object).m_cLocation = location;
	}
	//return (*current_Object).m_cLocation;
}
void Reflect(struct SGameChar *Shield, struct SGameChar *Bullet, COORD shield, COORD bullet)
{
	if (shield.X == bullet.X&&shield.Y == bullet.Y)
	{
		(*Bullet).isreflected = true;
		(*Bullet).direction = (*Shield).direction;
		(*Bullet).m_cLocation=(*Shield).m_cLocation;
	}
}

//void Bullet()
//{
//	g_proj.m_cLocation.X = z;
//	g_proj.m_cLocation.Y = i;
//	g_proj.m_bActive = true;
//}