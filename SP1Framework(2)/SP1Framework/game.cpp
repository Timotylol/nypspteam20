﻿// This is the main file for the game logic and function

#include "game.h"
#include "Framework\console.h"
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <vector>
#include <io.h>
#include <fcntl.h>
#include <fstream>
#include<cstdlib>
#include <stdlib.h> 
#include <ctime>
#include <stdio.h> 
#include <Windows.h>
#pragma comment(lib, "winmm.lib")

using namespace std;
int healths = 5;
bool gotHit;
int score = 0;
double g_hiddenElapsedTime;
double  g_dElapsedTime;
double g_dElapsedPauseTime;
double  g_dDeltaTime;
bool    g_abKeyPressed[K_COUNT];

//render
COORD c;
int HposX = 0;
int SposX = 0;
vector<string> Menu;
vector<string> Controls;
vector <string> Pause;
vector <string> Health;
vector <string> StatsBar;
vector <string> Shield;
vector <string> enemypath;
vector<string> currentPath;
vector <string> Defeat;
vector <string> Boss;
vector <string> Saves;
vector <vector<string>*> Alltxt = { &Menu,&Controls,&Pause,&Health,&StatsBar,&Shield,&enemypath,&currentPath,&Defeat,&Boss };

// Game specific variables here
SGameChar   g_sChar;
SGameChar   g_sShield;
SGameChar	g_exit;
SGameChar   g_Boss;
//vector<SGameChar>
vector<SGameChar> g_Enemy;
vector<SGameChar> g_proj;
vector<SGameChar*> AllGamechar;
//Game states
EGAMESTATES g_eGameState = S_SPLASHSCREEN;
ELEVELS g_eLevelState = LevelStart;
int movement_tracker = 0;
bool changedirection = false;
int frame = 0;

// Console object

Console g_Console(80, 30, "SP1 Framework");
Maps maps;

float Bspeed = 0.075;
int Enemycount = 0;
int bullet;
int ShieldDur;
bool ingame = false;
bool OpenGate;
int LevelNum = 1;
bool isboss;
vector<thread> _thread;
//--------------------------------------------------------------
// Purpose  : Initialisation function
//            Initialize variables, allocate memory, load data from file, etc. 
//            This is called once before entering into your main loop
// Input    : void
// Output   : void
//--------------------------------------------------------------

void init(void)
{
	isboss = false;
	gotHit = false;
	srand(time(NULL));
	g_proj.clear();
	g_Enemy.clear();
	enemypath.clear();
	AllGamechar.clear();
	AllGamechar.push_back(&g_sChar);
	AllGamechar.push_back(&g_sShield);
	ShieldDur = 10;
	g_dElapsedTime = 0.0;
	g_hiddenElapsedTime = 0.0;
	maps = Maps();
	maps.Map.clear();
	for (int i = 0; i < Alltxt.size(); i++)
	{
		(*Alltxt[i]).clear();
	}
	Enemycount = 0;
	//g_proj = SGameChar();
	GetFiles();
	MapProduce();
	// Set precision for floating point output
	// sets the initial state for the game
	for (int i = 0; i < maps.Map.size(); i++)
	{
		for (int z = 0; z < 80; z++)
		{
			switch (maps.Map[i][z])
			{
			case 'P':
				g_sChar.m_cLocation.X = z;
				g_sChar.m_cLocation.Y = i;
				g_sChar.m_bActive = true;
				g_sChar.char_s = 'P';
				break;
			case 'E':
				g_Enemy.push_back(SGameChar());
				g_Enemy[Enemycount].m_cLocation.X = z;
				g_Enemy[Enemycount].m_cLocation.Y = i;
				g_Enemy[Enemycount].m_bActive = true;
				g_Enemy[Enemycount].direction = -4;
				g_Enemy[Enemycount].char_s = 'E';
				Enemycount++;
				break;
			case 'N':
				g_exit.m_cLocation.X = z;
				g_exit.m_cLocation.Y = i;
				g_exit.char_s = 'N';
				g_exit.m_bActive = true;
			default:
				break;
			}
		}
	}
	g_sShield.char_s = 'S';
	//AllGamecharp.insert(AllGamecharp.end(), &g_Enemy.begin(), &g_Enemy.end());
	for (int i = 0; i < g_Enemy.size(); i++)
	{
		AllGamechar.push_back(&g_Enemy[i]);
	}
	for (int i = 0; i < AllGamechar.size(); i++)
	{
		(*AllGamechar[i]).bounceTime = 0;
		(*AllGamechar[i]).bounceTime2 = 0;
		if ((*AllGamechar[i]).char_s == 'E')
			(*AllGamechar[i]).bounceTime2 = rand() % 2 + 1;
	}
}

void GetFiles()
{
	Saves.clear();
	ifstream savefiles("Saves.txt");
	if (savefiles.is_open())
	{
		string line;
		while (getline(savefiles, line))
			Saves.push_back(line);
	}
	ifstream menufiles("Menu.txt");
	if (menufiles.is_open())
	{
		string line;
		while (getline(menufiles, line))
			Menu.push_back(line);
	}

	ifstream controlfiles("Controls.txt");
	if (controlfiles.is_open())
	{
		string line;
		while (getline(controlfiles, line))
			Controls.push_back(line);
	}

	ifstream pausefiles("Pause.txt");
	if (pausefiles.is_open())
	{
		string line;
		while (getline(pausefiles, line))
			Pause.push_back(line);
	}

	ifstream HealthsF("HealthIcon.txt");
	if (HealthsF.is_open())
	{
		string line;
		while (getline(HealthsF, line))
			Health.push_back(line);
	}
	ifstream StatsF("StatsBar.txt");
	if (StatsF.is_open())
	{
		string line;
		while (getline(StatsF, line))
			StatsBar.push_back(line);
	}
	ifstream ShieldF("Shield.txt");
	if (ShieldF.is_open())
	{
		string line;
		while (getline(ShieldF, line))
			Shield.push_back(line);
	}
	ifstream PathF("path.txt");
	if (PathF.is_open())
	{
		string line;
		while (getline(PathF, line))
			enemypath.push_back(line);
	}
	ifstream DefeatF("Defeat.txt");
	if (DefeatF.is_open())
	{
		string line;
		while (getline(DefeatF, line))
			Defeat.push_back(line);
	}
	ifstream Bossf("Boss.txt");
	if (Bossf.is_open())
	{
		string line;
		while (getline(Bossf, line))
			Boss.push_back(line);
	}
}
void MapProduce()
{
	switch (g_eLevelState)
	{
	case LevelStart:
		for (int i = 0; i < Saves.size(); i++)
		{
			for (int z = 0; z < Saves[i].size(); z++)
			{
				if (Saves[i][0] == 'L')
					if (Saves[i][z] == '=')
						LevelNum = (int)Saves[i][z + 1] - 48;
			}
		}
		break;
	case LevelRestart:
		LevelNum = LevelNum;
		break;
	case LevelEnd:
		ofstream savefiles("Saves.txt");
		LevelNum++;
		if (LevelNum == 4)
		{
			LevelNum = 1;
		}
		Saves[0].clear();
		Saves[0] += "Level=" + to_string(LevelNum);;
		if (savefiles.is_open())
		{
			savefiles.clear();
			for (int i = 0; i < Saves.size(); i++)
			{
				savefiles << Saves[0];
			}
		}
		break;
	}
	switch (LevelNum)//removes and restores Map data
	{
	case 1:
	{
		ifstream M1files("Map_1.txt");
		if (M1files.is_open())
		{
			string line;
			int i = 0;
			while (getline(M1files, line))
			{
				maps.Map.push_back(line);
				++i;
			}
		}
	}
	break;
	case 2:
	{
		ifstream M2files("Map_2.txt");
		if (M2files.is_open())
		{
			string line;
			int i = 0;
			while (getline(M2files, line))
			{
				maps.Map.push_back(line);
				++i;
			}
		}
	}
	break;
	case 3:
	{
		ifstream M3files("Map_3.txt");
		if (M3files.is_open())
		{
			string line;
			int i = 0;
			while (getline(M3files, line))
			{
				maps.Map.push_back(line);
				++i;
			}
		}
	}
	break;
	case 4:
	{
		isboss = true;
		ifstream Bfiles("BossMap.txt");
		if (Bfiles.is_open())
		{
			string line;
			int i = 0;
			while (getline(Bfiles, line))
			{
				maps.Map.push_back(line);
				++i;
			}
		}
	}
	break;
	}
}

//void ResetGame()
//{
//	g_dElapsedTime = 0.0;
//	g_hiddenElapsedTime = 0.0;
//	g_dBounceTime = 0.0;
//	g_dBounceTimeB = 0.0;
//
//	maps = Maps();
//}
//--------------------------------------------------------------
// Purpose  : Reset before exiting the program
//            Do your clean up of memory here
//            This is called once just before the game exits
// Input    : Void
// Output   : void
//--------------------------------------------------------------

void shutdown(void)
{
	// Reset to white text on black background
	colour(FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED);

	g_Console.clearBuffer();
}

//--------------------------------------------------------------
// Purpose  : Getting all the key press states
//            This function checks if any key had been pressed since the last time we checked
//            If a key is pressed, the value for that particular key will be true
//
//            Add more keys to the enum in game.h if you need to detect more keys
//            To get other VK key defines, right click on the VK define (e.g. VK_UP) and choose "Go To Definition" 
//            For Alphanumeric keys, the values are their ascii values (uppercase).
// Input    : Void
// Output   : void
//--------------------------------------------------------------

void getInput(void)
{

	g_abKeyPressed[WKey] = isKeyPressed(0x57);
	g_abKeyPressed[SKey] = isKeyPressed(0x53);
	g_abKeyPressed[AKey] = isKeyPressed(0x41);
	g_abKeyPressed[DKey] = isKeyPressed(0x44);
	g_abKeyPressed[K_UP] = isKeyPressed(VK_UP);
	g_abKeyPressed[K_DOWN] = isKeyPressed(VK_DOWN);
	g_abKeyPressed[K_LEFT] = isKeyPressed(VK_LEFT);
	g_abKeyPressed[K_RIGHT] = isKeyPressed(VK_RIGHT);
	g_abKeyPressed[K_SPACE] = isKeyPressed(VK_SPACE);
	g_abKeyPressed[K_ESCAPE] = isKeyPressed(VK_ESCAPE);
	g_abKeyPressed[K_BACKSPACE] = isKeyPressed(VK_BACK);
	g_abKeyPressed[K_PAUSE] = isKeyPressed(0x50);

}

//--------------------------------------------------------------
// Purpose  : Update function
//            This is the update function
//            double dt - This is the amount of time in seconds since the previous call was made
//
//            Game logic should be done here.
//            Such as collision checks, determining the position of your game characters, status updates, etc
//            If there are any calls to write to the console here, then you are doing it wrong.
//
//            If your game has multiple states, you should determine the current state, and call the relevant function here.
//
// Input    : dt = deltatime
// Output   : void
//--------------------------------------------------------------

void update(double dt)
{
	// get the delta time
	g_hiddenElapsedTime += dt;
	g_dElapsedTime += dt;
	g_dElapsedPauseTime += dt;
	g_dDeltaTime = dt;

	switch (g_eGameState)
	{
	case S_SPLASHSCREEN: splashScreenWait(); // game logic for the splash screen
		ingame = false;
		break;
	case S_GAME: gameplay(); // gameplay logic when we are in the game
		ingame = true;
		break;
	case S_CONTROLS: ControlsPage();
		break;
	case S_PAUSE: PauseScreen();
		break;
	case S_DEFEAT: DefeatScreen();
		break;
	}

}

//--------------------------------------------------------------
// Purpose  : Render function is to update the console screen
//            At this point, you should know exactly what to draw onto the screen.
//            Just draw it!
//            To get an idea of the values for colours, look at console.h and the URL listed there
// Input    : void
// Output   : void
//--------------------------------------------------------------

void UpdateMap(COORD PreviousCord, COORD newCord, char Characters)
{
	if (PreviousCord.Y < maps.Map.size())
	{
		maps.Map[PreviousCord.Y][PreviousCord.X] = ' ';
		maps.Map[newCord.Y][newCord.X] = Characters;
	}
}

void render()
{
	clearScreen();      // clears the current screen and draw from scratch 
	switch (g_eGameState)
	{
	case S_SPLASHSCREEN:
		renderScreen(Menu);
		healths = 5;
		break;
	case S_GAME: renderGame();
		break;
	case S_PAUSE:
		if (ingame)
			renderGame();
		renderScreen(Pause);
		break;
	case S_CONTROLS:
		if (ingame)
			renderGame();
		renderScreen(Controls);
		renderStatsBar();
		break;
	case S_DEFEAT:
		renderScreen(Defeat);
		renderScore();
		break;
	}
	if (StartGame() != true)
	{
		renderFramerate();  // renders debug information, frame rate, elapsed time, etc
		renderLevelNumber();
		renderHealth();
		renderScore();
	}
	renderToScreen();   // dump the contents of the buffer to the screen, one frame worth of game

}

void splashScreenWait()    // waits for time to pass in splash screen
{
	bool start = false;

	if (isKeyPressed(0x0D))	//starts game
	{
		g_eLevelState = LevelStart;
		init();
		PlaySound(TEXT("Fantasy_Game_Background.wav"), NULL, SND_ASYNC | SND_FILENAME | SND_LOOP);
		g_hiddenElapsedTime = 0.0;
		g_dElapsedTime = 0.0;	//timer resets to 0		
		start = true;
	}
	if (isKeyPressed(0x31))	//starts game
	{
		g_eLevelState = LevelStart;
		init();
		LevelNum = 1;
		ofstream savefiles("Saves.txt");
		Saves[0].clear();
		Saves[0] += "Level=" + to_string(1);;
		if (savefiles.is_open())
		{
			savefiles.clear();
			for (int i = 0; i < Saves.size(); i++)
			{
				savefiles << Saves[0];
			}
		}
		g_eLevelState = LevelStart;
		init();
		PlaySound(TEXT("Fantasy_Game_Background.wav"), NULL, SND_ASYNC | SND_FILENAME | SND_LOOP);
		g_hiddenElapsedTime = 0.0;
		g_dElapsedTime = 0.0;	//timer resets to 0		
		start = true;
	}
	if (start == true) // waits for player input to start game, else do nothing
	{
		g_eGameState = S_GAME;
		//		COORD c;
		COORD cons = g_Console.getConsoleSize();
	}
	else if (isKeyPressed(0x32))// waits for player input to go to controls, else do nothing
	{
		g_eGameState = S_CONTROLS;
	}
	if (g_abKeyPressed[K_ESCAPE])
	{
		g_bQuitGame = true;
	}
}

void ControlsPage()
{

	processUserInput(); // checks if you should change states or do something else with the game, e.g. pause, exit
	moveCharacter();    // moves the character, collision detection, physics, etc
								// sound can be played here too.
	if (g_abKeyPressed[K_ESCAPE])
	{
		g_bQuitGame = true;
	}

	else if (g_abKeyPressed[K_BACKSPACE])
	{
		g_eGameState = S_SPLASHSCREEN;
	}
	//processUserInput(); // checks if you should change states or do something else with the game, e.g. pause, exit
	//moveCharacter();    // moves the character, collision detection, physics, etc
	//         					// sound can be played here too.

}

void PauseScreen()
{
	if (isKeyPressed(0x33))
	{
		g_eGameState = S_SPLASHSCREEN;
		healths = 5;
		score = 0;
	}
	else if (isKeyPressed(0x34))
	{
		g_dElapsedTime -= g_dElapsedPauseTime;
		g_eGameState = S_GAME;
	}
	else if (isKeyPressed(0x35))
	{
		g_eLevelState = LevelRestart;
		healths = 5;
		score = 0;
		init();
		g_eGameState = S_GAME;
	}
	else if (g_abKeyPressed[K_ESCAPE])
	{
		g_bQuitGame = true;
	}
	if (isKeyPressed(0x32))
	{
		g_eGameState = S_CONTROLS;
	}

}

void DefeatScreen()
{

	if (isKeyPressed(0x33))
	{
		g_eGameState = S_SPLASHSCREEN;
		g_eLevelState = LevelStart;
		init();
		ofstream savefiles("Saves.txt");
		Saves[0].clear();
		Saves[0] += "Level=" + to_string(LevelNum);;
		if (savefiles.is_open())
		{
			savefiles.clear();
			for (int i = 0; i < Saves.size(); i++)
			{
				savefiles << Saves[0];
			}
		}
	}
	else if (g_abKeyPressed[K_ESCAPE])
	{
		g_bQuitGame = true;
	}

}

void HealthDrop()
{
	if (g_sChar.bounceTime2 > g_dElapsedTime);
	else
	{
		healths -= 1;
		g_sChar.bounceTime2 = g_dElapsedTime + 0.125;
		if (score >= 50)
			score -= 50;
		if (healths == 0)
		{
			//init();
			g_eGameState = S_DEFEAT;
		}
	}
}

void Sound(string Soundtoplay)
{

}

void gameplay()            // gameplay logic
{
	thread t2(deleteDestroyed);
	SetThread();
	processUserInput(); // checks if you should change states or do something else with the game, e.g. pause, exit
	thread t1(moveCharacter);   // moves the character, collision detection, physics, etc
	t1.join();
	t2.join();
	for (thread &t : _thread)
		if (t.joinable())
		{
			t.join();
			Sleep(0);
		}
}
void setAllGameChar()
{
	AllGamechar.clear();
	AllGamechar.push_back(&g_sChar);
	AllGamechar.push_back(&g_sShield);
	for (int i = 0; i < g_Enemy.size(); i++)
	{
		AllGamechar.push_back(&g_Enemy[i]);
	}
	for (int i = 0; i < g_proj.size(); i++)
	{
		AllGamechar.push_back(&g_proj[i]);
	}
}
void deleteDestroyed()
{
	/*for (int i = 0; i < AllGamechar.size(); i++)
	{
		if ((*AllGamechar[i]).isdestroyed == true)
		{
			maps.Map[(*AllGamechar[i]).m_cLocation.Y][(*AllGamechar[i]).m_cLocation.X] = ' ';
			AllGamechar.erase(AllGamechar.begin() + i);
		}
	}*/
	for (int i = 0; i < g_proj.size(); i++)
	{
		if ((g_proj[i]).isdestroyed == true)
		{
			maps.Map[g_proj[i].m_cLocation.Y][g_proj[i].m_cLocation.X] = ' ';
			g_proj.erase(g_proj.begin() + i);
		}
	}
	for (int i = 0; i < g_Enemy.size(); i++)
	{
		if ((g_Enemy[i]).isdestroyed == true)
		{
			maps.Map[g_Enemy[i].m_cLocation.Y][g_Enemy[i].m_cLocation.X] = ' ';
			g_Enemy.erase(g_Enemy.begin() + i);
		}
	}
	setAllGameChar();
	assigncount(g_Enemy.size());
}
bool StartGame()
{

	if (g_eGameState != S_GAME)
	{
		return true;
	}
	else
	{
		return false;
	}
}
void SetThread()
{
	_thread.clear();
	for (int i = 0; i < g_Enemy.size(); i++)
	{
		_thread.push_back(thread(moveEnemy, &g_Enemy[i]));
	}
	for (int i = 0; i < g_proj.size(); i++)
	{
		_thread.push_back(thread(moveBullet, i));
	}
}
void moveEnemy(struct SGameChar *EnemyC)
{

	float speed = 0.15;
	if (((*EnemyC).direction / 2) % 2 != 0)
	{
		switch (enemypath[(*EnemyC).m_cLocation.Y + (*EnemyC).direction / 2 + 25 * (LevelNum - 1)][(*EnemyC).m_cLocation.X])
		{
		case 'L':
			(*EnemyC).direction = -4;
			break;
		case 'R':
			(*EnemyC).direction = 4;
			break;
		case 'U':
			(*EnemyC).direction = -2;
			break;
		case 'D':
			(*EnemyC).direction = 2;
			break;
		default:
			break;
		}
	}
	else
	{
		switch (enemypath[(*EnemyC).m_cLocation.Y + 25 * (LevelNum - 1)][(*EnemyC).m_cLocation.X + (*EnemyC).direction / 4])
		{
		case 'L':
			(*EnemyC).direction = -4;
			break;
		case 'R':
			(*EnemyC).direction = 4;
			break;
		case 'U':
			(*EnemyC).direction = -2;
			break;
		case 'D':
			(*EnemyC).direction = 2;
			break;
		default:
			break;
		}
	}
	COORD preCord = (*EnemyC).m_cLocation;
	movement(&(*EnemyC).direction, &(*EnemyC), speed, g_dElapsedTime, &(*EnemyC).bounceTime, &maps, AllGamechar);
	UpdateMap(preCord, (*EnemyC).m_cLocation, 'E');
	SpawnBullet(&(*EnemyC), &(*EnemyC).bounceTime2);

}
void SpawnBullet(struct SGameChar *EnemyC, double *timeBtAtks)
{
	bool canshoot = false;
	if (*timeBtAtks > g_dElapsedTime)
	{
	}
	else
	{
		if (((*EnemyC).direction / 2) % 2 != 0)
		{
			if (maps.Map[(*EnemyC).m_cLocation.Y + (*EnemyC).direction / 2][(*EnemyC).m_cLocation.X] == ' ')
				canshoot = true;
		}
		else
			if (maps.Map[(*EnemyC).m_cLocation.Y][(*EnemyC).m_cLocation.X + (*EnemyC).direction / 4] == ' ')
				canshoot = true;
		if (canshoot)
		{
			g_proj.push_back(SGameChar());
			g_proj[g_proj.size() - 1].m_cLocation = (*EnemyC).m_cLocation;
			g_proj[g_proj.size() - 1].direction = (*EnemyC).direction;
			g_proj[g_proj.size() - 1].char_s = 'B';
			if (((*EnemyC).direction / 2) % 2 != 0)
			{
				g_proj[g_proj.size() - 1].m_cLocation.Y = (*EnemyC).m_cLocation.Y + (*EnemyC).direction / 2;
			}
			*timeBtAtks = g_dElapsedTime + 1;
		}
	}
}
void moveBullet(int bullets)
{
	g_proj[bullets].m_cLocation;
	/*struct SGameChar *ProjC;
	struct SGameChar *EnemyC;*/
	if (g_proj.size()!=0)
	{
		COORD preCord = g_proj[bullets].m_cLocation;
		movement(&g_proj[bullets].direction, &g_proj[bullets], Bspeed, g_hiddenElapsedTime, &g_proj[bullets].bounceTime, &maps, AllGamechar);
		UpdateMap(preCord, g_proj[bullets].m_cLocation, 'B');
		Reflect(&g_sShield, &g_proj[bullets], g_sShield.m_cLocation, g_proj[bullets].m_cLocation);
	}
}

void moveCharacter()
{

	COORD preCord = g_sChar.m_cLocation;
	int direction = 0;
	float speed = 0.05;
	if (g_abKeyPressed[WKey])
		direction = -2;

	else if (g_abKeyPressed[SKey])
		direction = 2;

	else if (g_abKeyPressed[AKey])
		direction = -4;

	else if (g_abKeyPressed[DKey])
		direction = 4;
	movement(&direction, &g_sChar, speed, g_hiddenElapsedTime, &g_sChar.bounceTime, &maps, AllGamechar);
	if (g_abKeyPressed[K_UP])
		ShieldPos(g_sShield.direction = -2);

	else if (g_abKeyPressed[K_DOWN])
		ShieldPos(g_sShield.direction = 2);

	else if (g_abKeyPressed[K_LEFT])
		ShieldPos(g_sShield.direction = -4);

	else if (g_abKeyPressed[K_RIGHT])
		ShieldPos(g_sShield.direction = 4);

	else
		ShieldPos(g_sShield.direction = 0);

	UpdateMap(preCord, g_sChar.m_cLocation, 'P');

}
bool playonce = false;
void ShieldPos(int direction)
{

	if (g_sShield.m_cLocation.X > 0)
		maps.Map[g_sShield.m_cLocation.Y][g_sShield.m_cLocation.X] = ' ';
	if (direction != 0)
	{

		g_sShield.m_cLocation = g_sChar.m_cLocation;
		if (direction / 2 % 2 != 0)
		{
			g_sShield.m_cLocation.Y = g_sChar.m_cLocation.Y + direction / 2;
		}
		else
		{
			g_sShield.m_cLocation.X = g_sChar.m_cLocation.X + direction / 4;
		}
	}
	else
	{
		if (g_sShield.bounceTime2 > g_dElapsedTime);
		else
		{
			g_sShield.bounceTime2 = g_dElapsedTime + 0.1;
			if (ShieldDur < 10)
				ShieldDur += 1;
		}
		g_sShield.m_cLocation.X = -1;
	}
	if (g_sShield.m_cLocation.X > 0)
	{
		if (maps.Map[g_sShield.m_cLocation.Y][g_sShield.m_cLocation.X] != ' '&& maps.Map[g_sShield.m_cLocation.Y][g_sShield.m_cLocation.X] != 'B')
		{
			g_sShield.m_cLocation.X = -1;
			playonce = true;
		}
		else
		{
			if (ShieldDur > 0)
			{

				if (g_sShield.bounceTime > g_dElapsedTime);
				else
				{
					g_sShield.bounceTime = g_dElapsedTime + 0.2;
					if (ShieldDur > 0)
						ShieldDur -= 1;
				}
				maps.Map[g_sShield.m_cLocation.Y][g_sShield.m_cLocation.X] = 'S';
			}
			else
			{
				g_sShield.m_cLocation.X = -1;
			}
		}
	}
}
void processUserInput()
{
	/*if (g_abKeyPressed[K_ESCAPE])
	{
		g_bQuitGame = true;
	}*/
	// pauses the game if player hits the 'P' key
	if (g_abKeyPressed[K_PAUSE])
	{
		g_dElapsedPauseTime = 0.0; //used to pause the game timer when player pauses game
		g_eGameState = S_PAUSE;
	}
	if (g_Enemy.size() == 0 && g_abKeyPressed[K_SPACE] && g_sChar.m_cLocation.X == g_exit.m_cLocation.X && g_sChar.m_cLocation.Y == g_exit.m_cLocation.Y)
	{
		g_eLevelState = LevelEnd;
		g_eGameState = S_GAME;
		if (gotHit == true)
		{
			score += 150;
		}
		else
		{
			score += 200;
		}
		healths++;
		init();
	}
}
void clearScreen()
{
	// Clears the buffer with this colour attribute
	g_Console.clearBuffer(0x0F);
}
void renderGame()
{
	for (int i = 0; i < maps.Map.size(); i++)
	{
		for (int z = 0; z < maps.Map[0].size(); z++)
		{
			c.X = z;
			c.Y = i;
			switch (maps.Map[i][z])
			{
			case 'x':
				g_Console.writeToBuffer(c, (char)176, 0xFf);
				break;
			case 'P':
				g_Console.writeToBuffer(c, (char)3, 0x0A);
				//g_Console.writeToBuffer(c, (char)1, 0x2F);
				break;
			case 'S':
				g_Console.writeToBuffer(c, (char)178, 0xf6);
				break;
			case 'E':
				g_Console.writeToBuffer(c, (char)64, 0x04);
				//g_Console.writeToBuffer(c, (char)2, 0x24);
				break;
			case ' ':
				g_Console.writeToBuffer(c, '.', 0x0F);
				break;
			case 'y':
				if (g_Enemy.size() == 0)
				{
					g_Console.writeToBuffer(c, (char)176, 0x2F);
				}
				else
				{
					g_Console.writeToBuffer(c, (char)177, 0x45);
				}

				break;
			case '=':
				g_Console.writeToBuffer(c, (char)177, 0x07);
				break;
			case '-':
				g_Console.writeToBuffer(c, (char)178, 0x07);
				break;
			default:
				g_Console.writeToBuffer(c, '.', 0x0F);
				//g_Console.writeToBuffer(c, "\4", 0x24);
				break;
			}
		}
	}
	c = g_exit.m_cLocation;
	if (g_Enemy.size() == 0)
	{
		g_Console.writeToBuffer(c, (char)1, 0x02);
	}
	else
	{
		g_Console.writeToBuffer(c, (char)2, 0x05);
	}
	for (int i = 0; i < g_proj.size(); i++)
	{
		c = g_proj[i].m_cLocation;
		if (g_proj[i].isreflected == true)
			g_Console.writeToBuffer(c, (char)248, 0x3F);
		else
			g_Console.writeToBuffer(c, (char)248, 0x8F);
	}
	renderStatsBar();
	renderHealthPoints();
	renderShield();

}

void renderStatsBar()
{
	for (int y = 0; y < StatsBar.size(); y++)
	{
		for (int z = 0; z < StatsBar[0].size(); z++)
		{
			c.X = z;
			c.Y = maps.Map.size() + y;
			switch (StatsBar[y][z])
			{
			case 'S':
				SposX = z;
				break;
			case 'H':
				HposX = z;
				break;
			case 'x':
				g_Console.writeToBuffer(c, (char)254, 0x90);
				break;
			case '|':
				g_Console.writeToBuffer(c, (char)179, 0x90);
				break;
			default:
				g_Console.writeToBuffer(c, StatsBar[y][z], 0x04);
				break;
			}
		}
	}
}

void renderHealthPoints()
{
	for (int x = 0; x < healths; x++)
	{
		for (int z = 0; z < Health.size(); z++)
		{
			c.X = HposX + x * Health[0].size();
			c.Y = maps.Map.size() + 1 + z;
			g_Console.writeToBuffer(c, Health[z], 0x02);
		}
	}
}

void renderShield()
{
	for (int i = 0; i < Shield.size() / 11; i++)
	{
		for (int z = 0; z < Shield[0].size(); z++)
		{
			int anim = -(ShieldDur - 10) * 3;
			c.X = z + SposX;
			c.Y = maps.Map.size() + 1 + i;
			if (anim < 33)
				switch (Shield[i + anim][z])
				{
				case '=':
					g_Console.writeToBuffer(c, (char)178, 0x07);
					break;
				case '-':
					g_Console.writeToBuffer(c, (char)177, 0x07);
					break;
				default:
					g_Console.writeToBuffer(c, Shield[i + anim][z], 0x03);
					break;
				}
		}
	}
	if (isboss)
	{
		for (int i = 0; i < Boss.size() / 4; i++)
		{
			for (int z = 0; z < Boss[0].size(); z++)
			{
				c.X = maps.Map[0].size() / 2 - Boss[0].size() / 2 + z;
				c.Y = 1 + i;
				switch (Boss[i + frame*Boss.size() / 4][z])
				{
				case ' ':
					break;
				case '.':
					g_Console.writeToBuffer(c, char(178), 0x04);
					break;
				case 'z':
					g_Console.writeToBuffer(c, ' ', 0x04);
					break;
				case '=':
					g_Console.writeToBuffer(c, char(177), 0x04);
					break;
				case '_':
					g_Console.writeToBuffer(c, char(176), 0x04);
					break;
				case '|':
					g_Console.writeToBuffer(c, char(186), 0x04);
					break;
				case '-':
					g_Console.writeToBuffer(c, '=', 0x04);
					break;
				default:
					g_Console.writeToBuffer(c, Boss[i + frame*Boss.size() / 4][z], 0x04);
					break;
				}
			}
		}
	}
	framechange();
}
double timesss;
void framechange()
{
	if (timesss > g_dElapsedTime);
	else
	{
		frame++;
		if (frame == 4)
			frame = 0;
		timesss = g_dElapsedTime + 0.2;
	}
}

void renderScreen(vector<string> Screen)
{
	COORD c;
	int centreX = maps.Map[0].size() - Screen[0].size();
	int centreY = maps.Map.size() - Screen.size();
	for (int i = 0; i < Screen.size(); ++i)
	{
		for (int j = 0; j < Screen[0].size(); j++)
		{
			c.X = j + centreX / 2;
			c.Y = i + centreY / 2;
			g_Console.writeToBuffer(c, Screen[i][j], 0x0F);
		}
	}
}
void renderHealth()
{

	COORD c;
	std::ostringstream ss;
	ss << "Health";
	c.X = 0;
	c.Y = 24;
	g_Console.writeToBuffer(c, ss.str(), 0xf0);

}
void renderScore()
{
	COORD c;
	std::ostringstream ss;
	ss << "Score: " << score;
	c.X = g_Console.getConsoleSize().X / 2 - 7;
	if (healths != 0)
	{
		c.Y = 0;
		g_Console.writeToBuffer(c, ss.str(), 0xF0);
	}
	else
	{
		c.Y = 12;
		g_Console.writeToBuffer(c, ss.str(), 0x0F);
	}


}
void renderLevelNumber()//
{

	COORD c;
	std::ostringstream ss;
	// displays the level number
	switch (LevelNum)
	{
	case 1:
	{
		ss << "Level 1";
		c.X = 72;
		c.Y = 24;
		g_Console.writeToBuffer(c, ss.str(), 0xf0);
	}
	break;
	case 2:
	{
		ss << "Level 2";
		c.X = 72;
		c.Y = 24;
		g_Console.writeToBuffer(c, ss.str(), 0xf0);
	}
	break;
	case 3:
	{
		ss << "Level 3";
		c.X = 72;
		c.Y = 24;
		g_Console.writeToBuffer(c, ss.str(), 0xf0);
	}
	break;
	}

}

void renderFramerate()
{

	COORD c;
	std::ostringstream ss;

	// displays the framerate
	ss << std::fixed << std::setprecision(3);
	ss << 1.0 / g_dDeltaTime << "fps";
	c.X = g_Console.getConsoleSize().X - 9;
	c.Y = 0;
	g_Console.writeToBuffer(c, ss.str(), 0xf0);

	// displays the elapsed time
	ss.str("");
	ss << g_dElapsedTime << "secs";
	c.X = 0;
	c.Y = 0;
	g_Console.writeToBuffer(c, ss.str(), 0xf0);

}

void renderToScreen()
{

	// Writes the buffer to the console, hence you will see what you have written
	g_Console.flushBufferToConsole();

}
