#ifndef _GAME_H
#define _GAME_H

#include <vector>
#include "Framework\timer.h"
#include "rendering.h"
#include "physics.h"
#include <thread>

using namespace std;
extern CStopWatch g_swTimer;
extern bool g_bQuitGame;
extern bool g_bRestartGame;

// Enumeration to store the control keys that your game will have
enum EKEYS
{
	K_UP,
	K_DOWN,
	K_LEFT,
	K_RIGHT,
	K_ESCAPE,
	K_SPACE,
	WKey,
	AKey,
	SKey,
	DKey,
	BKey,
	K_BACKSPACE,
	K_PAUSE,
	K_COUNT,
};

// Enumeration for the different screen states
enum EGAMESTATES
{
	S_MENU,
    S_SPLASHSCREEN,
    S_GAME,
	S_CONTROLS,
	S_PAUSE,
	S_DEFEAT,
    S_COUNT,
};

//Enumeration for the different level conditions
enum ELEVELS
{
	LevelStart,
	LevelRestart,
	LevelEnd,
};

//struct for the map
struct Maps
{
	std::vector<std::string> Map;
};

// struct for the game character
struct SGameChar
{
	char char_s;
	int direction;
	COORD m_cLocation;
	bool  m_bActive;
	double bounceTime;
	double bounceTime2;
	bool isdestroyed = false;
	bool isreflected;
};
void framechange();
//main
void init        ( void );      // initialize your variables, allocate memory, etc
void getInput    ( void );      // get input from player
void update      ( double dt ); // update the game and the state of the game
void render      ( void );      // renders the current state of the game to the console
void shutdown    ( void );      // do clean up, free memory
// game initialise
void GetFiles();			// makes vector strings from txt files except for map
void splashScreenWait();    // waits for input in splash screen
void gameplay();            // gameplay logic
void ControlsPage();		//
void PauseScreen();			//
void DefeatScreen();		// different screen states which take in different input
void renderHealth();		//
void moveCharacter();       // moves the character, collision detection, physics, etc
void movement(int *direction, struct SGameChar *current_Object, float speed, double elapsedT, double *bounceT, struct Maps *maps, vector<SGameChar*> AllChar);
void MapProduce();		    // gets level state and produces a map
void processUserInput();    // checks if you should change states or do something else with the game, e.g. pause, exit
void HealthDrop();			// player health logic
// miscellanous logic
bool StartGame();
void Sound( std::string Soundtoplay );
void deleteDestroyed();
#endif // _GAME_H