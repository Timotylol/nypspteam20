#ifndef _PHYSICS_H
#define _PHYSOCS_H

#include "Framework\timer.h"


//Physics
void moveCharacter();       // moves the character, collision detection, physics, etc
void ShieldPos(int direction);	// moves the player's shield
void moveBullet(int bullets);	// moves projectile
void moveEnemy(struct SGameChar *EnemyC);	//moves enemy in scripted path
void SpawnBullet(struct SGameChar *EnemyC, double *timeBtAtks); //
void SetThread();				//
void assigncount(int size);			// tests for collision between two units
void Reflect(struct SGameChar *Shield, struct SGameChar *Bullet, COORD shield, COORD bullet); // redirects the enemy projectile using shield
#endif
