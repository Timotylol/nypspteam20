#ifndef _RENDER_H
#define _RENDER_H

#include <vector>
#include "Framework\timer.h"



void clearScreen();         // clears the current screen and draw from scratch 
void renderScreen(std::vector<std::string> Screen); //renders non-game screens
void renderHealth();		// renders health points
void renderGame();          // renders the game stuff
void renderStatsBar();		// renders status bar
void renderHealthPoints();	// renders player's health points
void renderShield();
void renderFramerate();     // renders debug information, frame rate, elapsed time, etc
void renderLevelNumber();	// renders number of level
void renderScore();			// renders player's score
void renderToScreen();      // dump the contents of the buffer to the screen, one frame worth of game
void UpdateMap(COORD PreviousCord, COORD newCord, char Characters);	// replaces next empty space with unit

#endif